package com.example.nizomjon.freelancerproject;

import android.app.Application;

import com.example.nizomjon.freelancerproject.module.MainModule;

import dagger.ObjectGraph;

/**
 * Created by Nizomjon on 12/7/16.
 */

public class App extends Application {

    private ObjectGraph graph;

    @Override
    public void onCreate() {
        super.onCreate();

        graph = ObjectGraph.create(new MainModule(this));
        inject(this);

    }

    public void inject(Object object) {
        try {
            graph.inject(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
