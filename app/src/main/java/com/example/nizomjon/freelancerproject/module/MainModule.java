package com.example.nizomjon.freelancerproject.module;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.nizomjon.freelancerproject.App;
import com.example.nizomjon.freelancerproject.utils.Settings;
import com.example.nizomjon.freelancerproject.activities.AddUserActivity;
import com.example.nizomjon.freelancerproject.activities.AdminActivity;
import com.example.nizomjon.freelancerproject.activities.BaseActivity;
import com.example.nizomjon.freelancerproject.activities.LoginActivity;
import com.example.nizomjon.freelancerproject.activities.UserDetailActivity;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Nizomjon on 12/7/16.
 */

@Module(
        library = true,
        injects = {
                App.class,
                BaseActivity.class,
                LoginActivity.class,
                AddUserActivity.class,
                UserDetailActivity.class,
                AdminActivity.class
        }
)

public class MainModule {

    private App application;


    public MainModule(App application) {
        this.application = application;
    }


    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Provides
    @Singleton
    Settings provideSettings(SharedPreferences preferences) {
        return new Settings(preferences);
    }

}
