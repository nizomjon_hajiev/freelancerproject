package com.example.nizomjon.freelancerproject.activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nizomjon.freelancerproject.R;
import com.example.nizomjon.freelancerproject.database.DatabaseHelper;
import com.example.nizomjon.freelancerproject.event.UserEvent;
import com.example.nizomjon.freelancerproject.model.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Nizomjon on 12/7/16.
 */

public class AddUserActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {
    public static final int REQUEST_ADD_USER = 101;
    @BindView(R.id.firstName)
    EditText firstNameT;
    @BindView(R.id.lastName)
    EditText lastNameT;
    @BindView(R.id.username)
    EditText userNameT;
    @BindView(R.id.password)
    EditText passwordT;
    @BindView(R.id.gender)
    Spinner genderT;
    @BindView(R.id.department)
    EditText departmentT;
    @BindView(R.id.birthday)
    TextView birthdayT;

    DatabaseHelper databaseHelper;
    Calendar myCalendar;
    String gander;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        databaseHelper = DatabaseHelper.getInstance(this);
        myCalendar = Calendar.getInstance();

        List<String> list = new ArrayList<String>();
        list.add("Male");
        list.add("Female");


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, list);

        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);

        genderT.setAdapter(dataAdapter);
    }

    @OnClick(R.id.birthday)
    void clickBirth() {

        new DatePickerDialog(AddUserActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {

        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        birthdayT.setText(sdf.format(myCalendar.getTime()));
    }


    // add new user to database
    @OnClick(R.id.add_user_btn)
    void addUser() {
        String firstName = firstNameT.getText().toString();
        String lastName = lastNameT.getText().toString();
        String userName = userNameT.getText().toString();
        String password = passwordT.getText().toString();
//        String gender = genderT.getText().toString();
        String birthday = birthdayT.getText().toString().trim();

        String department = departmentT.getText().toString();

        User user = new User(databaseHelper.getNextID(), firstName, lastName, userName, password, (String) genderT.getSelectedItem(), birthday, department);

        // check this user is not available in database
        if (!databaseHelper.isExistUser(userName)) {
            databaseHelper.addUser(user);
            postEvent(UserEvent.refresh());
            finish();
        } else {
            Toast.makeText(this, "This user exist please add another one", Toast.LENGTH_SHORT).show();
        }
    }

    public void onEvent(UserEvent e) {

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        gander = (String) adapterView.getItemAtPosition(i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        gander = "Male";
    }
}
