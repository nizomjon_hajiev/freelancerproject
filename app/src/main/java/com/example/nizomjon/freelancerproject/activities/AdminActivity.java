package com.example.nizomjon.freelancerproject.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.nizomjon.freelancerproject.R;
import com.example.nizomjon.freelancerproject.adapter.UserAdapter;
import com.example.nizomjon.freelancerproject.database.DatabaseHelper;
import com.example.nizomjon.freelancerproject.event.UserEvent;
import com.example.nizomjon.freelancerproject.model.User;
import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Nizomjon on 12/7/16.
 */

public class AdminActivity extends BaseActivity {

    public static final int REQUEST_ADD_USER = 101;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.float_action_button)
    TextView fab;
    @BindView(R.id.no_users_view)
    TextView noUserView;

    DatabaseHelper databaseHelper;
    UserAdapter userAdapter;
    List<User> users;
    Gson gson;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        databaseHelper = DatabaseHelper.getInstance(this);
        gson = new Gson();
        reloadView();

    }

    // reload all users list from database
    private void reloadView() {
        users = databaseHelper.getAllUsers();
        if (users.size() > 0) {
            userAdapter = new UserAdapter(users, this, clickItemListener);
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            recyclerView.setAdapter(userAdapter);
            noUserView.setVisibility(View.GONE);
        } else {
            userAdapter = new UserAdapter(users, this, clickItemListener);
            userAdapter.clear();
            recyclerView.setVisibility(View.GONE);
            noUserView.setVisibility(View.VISIBLE);
        }
    }

    // edit to see user details

    UserAdapter.ClickItemListener clickItemListener = new UserAdapter.ClickItemListener() {
        @Override
        public void edit(User user, int position) {
            String userBody = gson.toJson(user);
            Intent intent = new Intent(AdminActivity.this, UserDetailActivity.class);
            intent.putExtra("user_body", userBody);
            startActivity(intent);
        }

        @Override
        public void delete(final User user, final int position) {
            new AlertDialog.Builder(AdminActivity.this)
                    .setTitle("Delete entry")
                    .setMessage("Are you sure you want to delete this user?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (databaseHelper.isExistUser(user.getUserName().trim())) {
                                userAdapter.deleteItem(position);
                                databaseHelper.deleteUser(user.getId());
                                postEvent(UserEvent.refresh());
                            }
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    };

    // add new user
    @OnClick(R.id.float_action_button)
    void addUser() {
        Intent intent = new Intent(AdminActivity.this, AddUserActivity.class);
        startActivityForResult(intent, REQUEST_ADD_USER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ADD_USER) {
//            reloadView();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.admin_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            if (databaseHelper != null) {
//                databaseHelper.removeAllUsers();
                Intent intent = new Intent(AdminActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // catch event
    public void onEvent(UserEvent e) {
        if (e.getType() == UserEvent.Type.USER_DATA_CHANGED) {
            reloadView();
        }
    }
}
