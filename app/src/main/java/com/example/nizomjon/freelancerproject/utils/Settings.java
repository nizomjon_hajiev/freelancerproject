package com.example.nizomjon.freelancerproject.utils;

import android.content.SharedPreferences;

import javax.inject.Singleton;

/**
 * Created by Nizomjon on 12/7/16.
 */

@Singleton
public class Settings {

    private SharedPreferences preferences;

    public Settings(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    public void saveAdminEmail(String email) {
        preferences.edit().putString("ADMIN_EMAIL", email).apply();
    }

    public String getAdminEmail() {
        return preferences.getString("ADMIN_EMAIL", null);
    }

    public void saveAdminPassword(String password) {
        preferences.edit().putString("ADMIN_PASSWORD", password).apply();
    }

    public String getAdminPassword() {
        return preferences.getString("ADMIN_PASSWORD", null);
    }
}
