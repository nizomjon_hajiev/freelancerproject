package com.example.nizomjon.freelancerproject.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.nizomjon.freelancerproject.database.DatabaseHelper;
import com.example.nizomjon.freelancerproject.R;
import com.example.nizomjon.freelancerproject.model.User;
import com.example.nizomjon.freelancerproject.event.UserEvent;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.nizomjon.freelancerproject.R.id.firstName;

/**
 * Created by Nizomjon on 12/7/16.
 */

public class UserDetailActivity extends BaseActivity {
    public static final int REQUEST_ADD_USER = 101;
    @BindView(firstName)
    EditText firstNameT;
    @BindView(R.id.lastName)
    EditText lastNameT;
    @BindView(R.id.username)
    EditText userNameT;
    @BindView(R.id.password)
    EditText passwordT;
    @BindView(R.id.gender)
    Spinner genderT;
    @BindView(R.id.department)
    EditText departmentT;
    @Nullable
    @BindView(R.id.add_user_btn)
    Button saveButton;
    @Nullable
    @BindView(R.id.delete_user_btn)
    Button deleteBtn;
    @Nullable
    @BindView(R.id.logout_btn)
    Button logoutBtn;
    @BindView(R.id.birthday)
    TextView birthdayT;

    DatabaseHelper databaseHelper;
    User user;
    Gson gson;
    Calendar myCalendar;
    User user1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        ButterKnife.bind(this);
        if (saveButton != null) {
            saveButton.setText("Save");
        }
        databaseHelper = new DatabaseHelper(this);
        myCalendar = Calendar.getInstance();
        gson = new Gson();
        user = gson.fromJson(getIntent().getStringExtra("user_body"), new TypeToken<User>() {
        }.getType());

        // fill fields with attributes
        firstNameT.setText(user.getFirstName());
        lastNameT.setText(user.getLastName());
        userNameT.setText(user.getUserName());
        passwordT.setText(user.getPassword());
        if (user.getGender().equals("Male")) {
            genderT.setSelection(0);
        } else {
            genderT.setSelection(1);
        }
        birthdayT.setText(user.getBirthday());
        departmentT.setText(user.getDepartment());


        // give option for simple user to change password
        if (settings.getAdminEmail() == null) {
            firstNameT.setEnabled(false);
            lastNameT.setEnabled(false);
            passwordT.setEnabled(true);
            userNameT.setEnabled(false);
            genderT.setEnabled(false);
            departmentT.setEnabled(false);
            birthdayT.setEnabled(false);
            deleteBtn.setVisibility(View.GONE);
            logoutBtn.setVisibility(View.VISIBLE);
        } else {
//            deleteBtn.setVisibility(View.VISIBLE);
            logoutBtn.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.logout_btn)
    void logout() {
        if (databaseHelper.isExistUser(userNameT.getText().toString().trim())) {
//            databaseHelper.deleteUser(user.getId());
            Intent intent = new Intent(UserDetailActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @OnClick(R.id.birthday)
    void clickBirth() {

        new DatePickerDialog(UserDetailActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {

        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        birthdayT.setText(sdf.format(myCalendar.getTime()));
    }

    // update user
    @OnClick(R.id.add_user_btn)
    void updateUser() {
        String firstName = firstNameT.getText().toString().trim();
        String lastName = lastNameT.getText().toString().trim();
        String userName = userNameT.getText().toString().trim();
        String password = passwordT.getText().toString().trim();
        String gender = (String) genderT.getSelectedItem();
        String birthday = birthdayT.getText().toString().trim();
        String department = departmentT.getText().toString().trim();

        user1 = new User(firstName, lastName, userName, password, gender, birthday, department);

        databaseHelper.updateUser(user1, user.getId());
        postEvent(UserEvent.refresh());
        finish();
    }


    // delete user
//    @OnClick(R.id.delete_user_btn)
//    void deleteUser() {
//        if (databaseHelper.isExistUser(userNameT.getText().toString().trim())) {
//            databaseHelper.deleteUser(user.getId());
//            postEvent(UserEvent.refresh());
//            finish();
//        }
//    }

    public void onEvent(UserEvent e) {

    }

}
