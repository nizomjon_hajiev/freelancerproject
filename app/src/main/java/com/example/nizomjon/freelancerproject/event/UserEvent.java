package com.example.nizomjon.freelancerproject.event;

/**
 * Created by Nizomjon on 12/7/16.
 */

public class UserEvent {

    public Type type;


    public static UserEvent refresh(){
        UserEvent event = new UserEvent();
        event.type=Type.USER_DATA_CHANGED;
        return event;
    }

   public enum Type{
        USER_DATA_CHANGED
    }

    public Type getType() {
        return type;
    }
}
