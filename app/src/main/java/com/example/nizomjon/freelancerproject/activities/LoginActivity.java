package com.example.nizomjon.freelancerproject.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nizomjon.freelancerproject.R;
import com.example.nizomjon.freelancerproject.database.DatabaseHelper;
import com.example.nizomjon.freelancerproject.event.UserEvent;
import com.example.nizomjon.freelancerproject.model.Admin;
import com.example.nizomjon.freelancerproject.model.User;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Shiv on 12/7/16.
 */

public class LoginActivity extends BaseActivity {

    @BindView(R.id.email)
    EditText emailT;
    @BindView(R.id.password)
    EditText passordT;

    public static final String ADMIN_USERNAME = "admin";
    public static final String ADMIN_PASSWORD = "admin";
    DatabaseHelper databaseHelper;

    int counter = 0;
    Gson gson;
    Admin admin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        databaseHelper = DatabaseHelper.getInstance(this);
        if (!databaseHelper.isExistAdmin("admin")) {
            databaseHelper.addAdmin();
            admin = databaseHelper.getAdmin();
        } else {
            admin = databaseHelper.getAdmin();
        }
        gson = new Gson();

    }

    // login function
    @OnClick(R.id.email_sign_in_button)
    void login() {

        if (!TextUtils.isEmpty(emailT.getText().toString()) && !TextUtils.isEmpty(passordT.getText().toString())) {
            //if the user is admin then authenticate and redirect to admin acivity
            if (emailT.getText().toString().equals(admin.getUsername()) && passordT.getText().toString().equals(admin.getPassword())) {
                // is username and password is correct move to Admin Screen
                settings.saveAdminEmail(emailT.getText().toString()); //admin email fetch
                settings.saveAdminPassword(passordT.getText().toString()); //admin password fetch
                Intent intent = new Intent(LoginActivity.this, AdminActivity.class);
                startActivity(intent);
                finish();

                //else just compare the user data and move to user view
            } else {
                String userName = emailT.getText().toString();
                String password = passordT.getText().toString();
                // checking counter if it is 4 we should close app
                if (counter <= 3) {

                    if (databaseHelper.getAllUsers().size() > 0) {
                        if (databaseHelper.isExistUser(userName)) {
                            User user = databaseHelper.getUser(userName);
                            if (user.getPassword().equals(password) && user.getUserName().equals(userName)) {
                                // is username and password is correct move to User Detail Screen
                                Intent intent = new Intent(LoginActivity.this, UserDetailActivity.class);
                                settings.saveAdminEmail(null);
                                settings.saveAdminPassword(null);
                                intent.putExtra("user_body", gson.toJson(user));
                                startActivity(intent);
                                finish();

                                //Exceptions or errors
                            } else {
                                // counting fails
                                counter++;
                                Toast.makeText(this, "Username or password incorrect", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            counter++;
                            Toast.makeText(this, "No this user", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        counter++;
                        Toast.makeText(this, "No user", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    finish();
                }
            }
        } else {
            Toast.makeText(this, "Please fill fields", Toast.LENGTH_SHORT).show();
        }

    }

    public void onEvent(UserEvent e) {

    }
}
