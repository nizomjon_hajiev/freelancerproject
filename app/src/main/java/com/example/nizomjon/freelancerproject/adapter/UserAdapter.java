package com.example.nizomjon.freelancerproject.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nizomjon.freelancerproject.R;
import com.example.nizomjon.freelancerproject.model.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nizomjon on 12/7/16.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    List<User> users;
    Context context;
    ClickItemListener clickItemListener;

    public UserAdapter(List<User> users, Context context, ClickItemListener clickItemListener) {
        this.users = users;
        this.context = context;
        this.clickItemListener = clickItemListener;
    }

    public UserAdapter(Context context, ClickItemListener clickItemListener) {

        this.context = context;
        this.clickItemListener = clickItemListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = users.get(position);
        holder.user = user;
        holder.userName.setText(user.getFirstName());
        holder.department.setText(user.getDepartment());

    }

    public void deleteItem(int position) {
        if (users != null) {
            users.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        if (users != null) {
            users.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_name)
        TextView userName;
        @BindView(R.id.department)
        TextView department;
        User user;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.edit_btn,R.id.user_container})
        void click() {
            if (clickItemListener != null && user != null) {
                clickItemListener.edit(user, getAdapterPosition());
            }
        }

        @OnClick(R.id.delete_btn)
        void delete() {
            if (clickItemListener != null && user != null) {
                clickItemListener.delete(user, getAdapterPosition());
            }
        }
    }

    public interface ClickItemListener {
        void edit(User user, int position);

        void delete(User user, int position);

    }
}
