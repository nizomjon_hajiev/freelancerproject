package com.example.nizomjon.freelancerproject.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.nizomjon.freelancerproject.model.Admin;
import com.example.nizomjon.freelancerproject.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shiv on 12/7/16.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "user";
    private static final String TABLE_USER = "table_user";
    private static final String TABLE_ADMIN = "table_admin";
    private static final String ADMIN_ID = "admin_id";
    private static final String ADMIN_PASSWORD = "admin_password";
    private static final String ADMIN_USERNAME = "admin_username";
    private static final String USER_ID = "id";
    private static final String USER_FIRST_NAME = "user_first_name";
    private static final String USER_LAST_NAME = "user_last_name";
    private static final String USER_USER_NAME = "user_user_name";
    private static final String USER_PASSWORD = "user_password";
    private static final String USER_GENDER = "user_gender";
    private static final String USER_BIRTHDAY = "user_birthday";
    private static final String USER_DEPARTMENT = "user_department";


    //creating users
    private static final String CREATE_USER = "create table " + TABLE_USER
            + " (" + USER_ID + " integer primary key autoincrement, "
            + USER_FIRST_NAME + " text,"
            + USER_LAST_NAME + " text,"
            + USER_USER_NAME + " text,"
            + USER_PASSWORD + " text,"
            + USER_GENDER + " text,"
            + USER_BIRTHDAY + " text,"
            + USER_DEPARTMENT + " text);";
    // creating admin table
    private static final String CREATE_ADMIN = "create table " + TABLE_ADMIN +
            " (" + ADMIN_ID + " integer primary key autoincrement, "
            + ADMIN_USERNAME + " text," + ADMIN_PASSWORD + " text);";

    private static DatabaseHelper instance;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DatabaseHelper getInstance(Context context) {
        instance = new DatabaseHelper(context);
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER);
        db.execSQL(CREATE_ADMIN);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    //add user to database

    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USER_ID, user.getId());
        values.put(USER_FIRST_NAME, user.getFirstName());
        values.put(USER_LAST_NAME, user.getLastName());
        values.put(USER_USER_NAME, user.getUserName());
        values.put(USER_PASSWORD, user.getPassword());
        values.put(USER_GENDER, user.getGender());
        values.put(USER_BIRTHDAY, user.getBirthday());
        values.put(USER_DEPARTMENT, user.getDepartment());

        db.insert(TABLE_USER, null, values);
        db.close();
    }

    public void addAdmin() {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ADMIN_USERNAME, "admin");
        values.put(ADMIN_PASSWORD, "admin");
        db.insert(TABLE_ADMIN, null, values);
        db.close();
    }

    public List<User> getAllUsers() {
        List<User> userList = new ArrayList<User>();
        String selectQuery = "SELECT  * FROM " + TABLE_USER;

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            try {
                if (cursor.moveToFirst()) {
                    do {
                        User user = new User();
                        user.setId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
                        user.setFirstName(cursor.getString(cursor.getColumnIndex(USER_FIRST_NAME)));
                        user.setLastName(cursor.getString(cursor.getColumnIndex(USER_LAST_NAME)));
                        user.setUserName(cursor.getString(cursor.getColumnIndex(USER_USER_NAME)));
                        user.setPassword(cursor.getString(cursor.getColumnIndex(USER_PASSWORD)));
                        user.setGender(cursor.getString(cursor.getColumnIndex(USER_GENDER)));
                        user.setBirthday(cursor.getString(cursor.getColumnIndex(USER_BIRTHDAY)));
                        user.setDepartment(cursor.getString(cursor.getColumnIndex(USER_DEPARTMENT)));
                        userList.add(user);
                    } while (cursor.moveToNext());
                }
            } finally {

                try {
                    cursor.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } finally {
            try {
                db.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return userList;
    }

    //check id user exist in database
    public boolean isExistUser(String username) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + TABLE_USER + " WHERE user_user_name = '" + username + "'", null);
        boolean exist = (cursor.getCount() > 0);
        cursor.close();
        sqLiteDatabase.close();
        return exist;
    }

    public boolean isExistAdmin(String username) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + TABLE_ADMIN + " WHERE admin_username = '" + username + "'", null);
        boolean exist = (cursor.getCount() > 0);
        cursor.close();
        sqLiteDatabase.close();
        return exist;
    }

    //get users from database in list

    public User getUser(String userName) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_USER + " WHERE user_user_name = '" + userName + "'", null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        User user = new User();
        user.setId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
        user.setFirstName(cursor.getString(cursor.getColumnIndex(USER_FIRST_NAME)));
        user.setLastName(cursor.getString(cursor.getColumnIndex(USER_LAST_NAME)));
        user.setUserName(cursor.getString(cursor.getColumnIndex(USER_USER_NAME)));
        user.setPassword(cursor.getString(cursor.getColumnIndex(USER_PASSWORD)));
        user.setGender(cursor.getString(cursor.getColumnIndex(USER_GENDER)));
        user.setBirthday(cursor.getString(cursor.getColumnIndex(USER_BIRTHDAY)));
        user.setDepartment(cursor.getString(cursor.getColumnIndex(USER_DEPARTMENT)));

        return user;
    }

    public Admin getAdmin() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_ADMIN, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        Admin admin = new Admin();
        admin.setId(cursor.getInt(cursor.getColumnIndex(ADMIN_ID)));
        admin.setUsername(cursor.getString(cursor.getColumnIndex(ADMIN_USERNAME)));
        admin.setPassword(cursor.getString(cursor.getColumnIndex(ADMIN_PASSWORD)));
        return admin;
    }

    //update data of users
    public void updateUser(User user, int id) {
        String where = "id=?";
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USER_FIRST_NAME, user.getFirstName());
        values.put(USER_LAST_NAME, user.getLastName());
        values.put(USER_USER_NAME, user.getUserName());
        values.put(USER_PASSWORD, user.getPassword());
        values.put(USER_GENDER, user.getGender());
        values.put(USER_BIRTHDAY, user.getBirthday());
        values.put(USER_DEPARTMENT, user.getDepartment());
        int count = db.update(TABLE_USER, values, where, new String[]{String.valueOf(id)});
        db.close();
    }

    //delete user from database

    public void deleteUser(int id) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            int count = db.delete(TABLE_USER, USER_ID + " = ?",
                    new String[]{String.valueOf(id)});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public int getNextID() {

        String selectQuery = "SELECT MAX(" + USER_ID +
                ") FROM " + TABLE_USER;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            int i = cursor.getInt(0) + 1;
            cursor.close();
            close();
            return i;
        } else {
            cursor.close();
            close();
            return 0;
        }
    }

}
